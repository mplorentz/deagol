# 2. Use Core Data For Persistence

Date: 2019-03-13

## Status

Accepted

## Context

We need to model the wiki locally and persist it for online access. One of the great features of Gollum is that it stores the pages in Git. Deagol should do that to. However, choosing a libgit wrapper, handling merge conflicts, and messing with ssh will be time consuming.

Core Data is widely used and I am familiar with it. This makes it a nice temporary solution to model entities.

## Decision

The MVP of Deagol will store wiki data in Core Data.

## Consequences

The Entity layer of Deagol will use Core Data to model and presist wiki pages. Core Data isn't the latest-and-greatest in persistence and there may be some issues integrating it with ReactiveCocoa.
