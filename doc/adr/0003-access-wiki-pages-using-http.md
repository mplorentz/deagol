# 3. Access Wiki Pages Using HTTP

Date: 2019-03-13

## Status

Accepted

## Context

Since the MVP will not integrate with libgit (see [ADR #2](0002-use-core-data-for-persistence.md)) we need another way to download and upload wiki pages.

## Decision

We will access wiki pages using the HTTP endpoints that Gollum's Ruby server provides.

## Consequences

We will implement an API layer to get wiki pages. It seems like this may involve scraping HTML from some endpoints like /files. Most or all of this work will be thrown away when we switch to libgit.
