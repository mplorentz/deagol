A native iOS client for [Gollum - A git-based Wiki](https://github.com/gollum/gollum).

Deagol is in alpha and is not feature complete.

# Building
If you would like to build and run Deagol yourself, see the "Project Setup" section in [CONTRIBUTING.md](CONTRIBUTING.md)

# Architecture

Architectural decisions are documented in the [doc/adr](doc/adr) directory of this repository.
