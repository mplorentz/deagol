//
//  DatastoreTests.swift
//  DeagolTests
//
//  Created by Matthew Lorentz on 3/23/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

@testable import Deagol
import XCTest
import CoreData

class DatastoreTests: XCTestCase {
    
    var context: NSManagedObjectContext!
    
    var sampleFiles: [File]!

    override func setUp() {
        let container = TestDataHelper.emptyInMemoryStore()
        context = container.viewContext
        sampleFiles = SampleDataGenerator.createSampleFiles(in: context)
    }
}
