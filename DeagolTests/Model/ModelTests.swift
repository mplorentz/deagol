//
//  DatabaseHelperTests.swift
//  DeagolTests
//
//  Created by Matthew Lorentz on 3/13/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

import XCTest
import CoreData
@testable import Deagol

class ModelTests: XCTestCase {
    
    /// An in-memory database that we will use for these tests
    var testContainer: NSPersistentContainer!

    override func setUp() {
        testContainer = TestDataHelper.emptyInMemoryStore()
    }
    
    /// Verifies that we can insert objects into our database and fetch them again
    func testFetchRequest() throws {
        // Arrange
        let testDirectory = Directory(context: testContainer.viewContext)

        // Act
        let directoryRequest = NSFetchRequest<Directory>(entityName: "Directory")
        let results = try testContainer.viewContext.fetch(directoryRequest)
        
        XCTAssertEqual(results.count, 1)
        XCTAssertEqual(results.first, testDirectory)
    }
    
    /// Tests that the SampleDataGenerator.addSampleData(to:) function actually adds some sample data.
    func testSampleData() throws {
        testContainer = TestDataHelper.inMemoryStoreWithSampleData()

        let directoryRequest = NSFetchRequest<Directory>(entityName: "Directory")
        let directories = try testContainer.viewContext.fetch(directoryRequest)
        
        XCTAssertEqual(directories.count, 1)

        let pageRequest = NSFetchRequest<Page>(entityName: "Page")
        let pages = try testContainer.viewContext.fetch(pageRequest)
        
        XCTAssertEqual(pages.count, 2)
    }
    
    /// Test that the SampleDataGenerator.isEmpty(container:) functions returns true when the container is empty.
    func testIsEmptyContainer_whenEmpty() throws {
        XCTAssertTrue(try testContainer.isEmpty())
    }
    
    /// Test that the SampleDataGenerator.isEmpty(container:) functions returns false when the container has data.
    func testIsEmptyContainer_whenNotEmpty() throws {
        // Arrange
        testContainer = TestDataHelper.inMemoryStoreWithSampleData()

        // Act & Assert
        XCTAssertFalse(try testContainer.isEmpty())
    }

    /// Tests the `path` property on `File`.
    func testPath() {
        let context = testContainer.viewContext
        let directory = Directory(context: context)
        directory.name = "sub directory"
        
        let subPage = Page(context: context)
        subPage.name = "sub page"
        subPage.parentDirectory = directory

        XCTAssertEqual(subPage.path, "sub directory/sub page")
        XCTAssertEqual(directory.path, "sub directory")
    }
}
