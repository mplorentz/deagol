//
//  TestDataHelper.swift
//  DeagolTests
//
//  Created by Matthew Lorentz on 3/14/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

@testable import Deagol
import CoreData
import XCTest

/// A set of functions to simplify setting up a Core Data stack with test data.
struct TestDataHelper {
    
    /// Creates an emtpy in-memory store.
    static func emptyInMemoryStore() -> DataStore {
        return DataStore(inMemory: true)
    }

    /// Creates an in-memory store with sample data from the `SampleDataGenerator`.
    static func inMemoryStoreWithSampleData() -> DataStore {
    
        let testContainer = emptyInMemoryStore()
        SampleDataGenerator.addSampleData(to: testContainer)
        
        return testContainer
    }
}
