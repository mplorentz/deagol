//
//  SampleDataGenerator.swift
//  Deagol
//
//  Created by Matthew Lorentz on 3/13/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

@testable import Deagol
import CoreData

struct SampleDataGenerator {
    
    /// Synchronously inserts sample data into the given container.
    static func addSampleData(to container: NSPersistentContainer) {
        let context = container.viewContext
        _ = SampleDataGenerator.createSampleFiles(in: context)
        try? context.save()
    }
    
    static func createSampleFiles(in context: NSManagedObjectContext) -> [File] {

        let indexPage = Page(context: context)
        indexPage.name = "index"
        indexPage.text = """
        # Index
        This is the home page.
        
        It has a link to a [sub-page](sub directory/sub page)
        """
        
        let subDirectory = Directory(context: context)
        subDirectory.name = "sub directory"
        
        let subPage = Page(context: context)
        subPage.name = "sub page"
        subPage.text = """
        This is the sub-page. It is in the sub directory.
        """
        
        subDirectory.children = Set([subPage]) as NSSet
        
        return [indexPage, subPage, subDirectory]
    }
}
