//
//  MockTableView.swift
//  DeagolTests
//
//  Created by Matthew Lorentz on 3/18/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

import UIKit

/// A table view that keeps track of how many times `reloadData()` has been called.
class MockTableView: UITableView {
    var reloadDataCallCount = 0
    
    override func reloadData() {
        reloadDataCallCount += 1
    }
}

