//
//  AppSystemIntegrationTests.swift
//  DeagolTests
//
//  Created by Matthew Lorentz on 3/20/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

@testable import Deagol
import XCTest
import ReactiveSwift
import ReactiveFeedback
import Result
import CoreData

class AppSystemIntegrationTests: XCTestCase {
    
    var context: NSManagedObjectContext!
    
    var testFiles: [File]!

    override func setUp() {
        context = TestDataHelper.emptyInMemoryStore().viewContext
        testFiles = SampleDataGenerator.createSampleFiles(in: context)
    }

    func _testBrowserSystemEventReturnsAppSystem() {
        // Arrange
        let (eventSignal, eventObserver) = Signal<BrowserSystem.Event, NoError>.pipe()
        let expectedPage = testFiles.first as! Page
        let browserSystem = BrowserSystem(initialState: BrowserSystem.State.initial(), userInput: eventSignal, api: API(host: ""), dataStore: DataStore(inMemory: true), navigationController: DeagolNavigationController())
        
        // Act
        eventObserver.send(value: .rowTapped(1))
        
        // Assert
        XCTAssertEqual(browserSystem.state.value.pageToShow, expectedPage)
    }
    
    func _testBindUIFeedback2() {
        let (eventSignal, eventObserver) = Signal<BrowserSystem.Event, NoError>.pipe()
        //        let feedback = BrowserSystem.bindUIFeedback(eventSignal: eventSignal)
        let expectedPage = testFiles.first as! Page
        
        let system = BrowserSystem(initialState: BrowserSystem.State.initial(), userInput: eventSignal, api: API(host: ""), dataStore: DataStore(inMemory: true), navigationController: DeagolNavigationController())

        
        
        let state = system.state!
        //        let expectation = self.expectation(description: "expectation")
        //        state.signal.take(first: 2).collect().observeValues { (state) in
        //            print(state)
        //            expectation.fulfill()
        //        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "Browser") as! BrowserViewController
        viewController.state = system.state
        viewController.viewDidLoad()
        
        // Act
        eventObserver.send(value: .rowTapped(1))
        
        
        //        waitForExpectations(timeout: 5, handler: nil)
        // Assert
        withExtendedLifetime(viewController) {
            eventually(XCTAssertEqual(state.value.pageToShow, expectedPage))
        }
    }
}
