//
//  PageViewControllerTests.swift
//  DeagolTests
//
//  Created by Matthew Lorentz on 3/21/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

@testable import Deagol
import XCTest
import ReactiveSwift
import ReactiveCocoa
import Result

/// A suite of tests for the `PageViewController`.
class PageViewControllerTests: XCTestCase {

    /// System-under-test
    var sut: PageViewController!
    
    /// The page that the view controller will display by default.
    var page: Page!
    
    override func setUp() {
        // Set up view controller
        sut = PageViewControllerTests.pageViewController()
        // Simulate the UISplitViewController.displayModeButtonItem
        sut.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Pages", style: .plain, target: nil, action: nil)
        _ = sut.view
        
        // Set up default state
        let context = TestDataHelper.emptyInMemoryStore().viewContext
        let files = SampleDataGenerator.createSampleFiles(in: context)
        page = files.first as? Page
        try! context.save()
        
        let defaultState = PageSystem.State(
            pagePath: page.path,
            savedText: page.text,
            isEditing: false,
            currentText: nil
        )
        sut.viewModelObserver.send(value: defaultState)
    }
    
    /// Instantiates the browser view controller from a storyboard
    static func pageViewController() -> PageViewController {
        
        let nav = UINavigationController()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "Page") as! PageViewController
        nav.viewControllers = [viewController]
        
        return viewController
    }
    
    /// Tests that the view controller has the correct title.
    func testTitle() {
        XCTAssertEqual(sut.title, "index")
        XCTAssertEqual(sut.navigationItem.title, "index")
    }
    
    /// Tests the default text of the title.
    func testTitleDefaultValue() {
        sut = PageViewControllerTests.pageViewController()
        _ = sut.view
        XCTAssertEqual(sut.title, nil)
        XCTAssertEqual(sut.navigationItem.title, nil)
    }

    /// Tests that the text view displays the page text.
    func testTextView() {
        XCTAssertEqual(sut.textView.text, page.text)
    }
    
    /// Tests the default text of the text view.
    func testTextViewDefaultValue() {
        sut = PageViewControllerTests.pageViewController()
        _ = sut.view
        XCTAssertEqual(sut.textView.text, "")
    }
    
    /// Test that the text view starts out in isEditing mode.
    func testTextViewNotEditableByDefault() {
        XCTAssertEqual(sut.textView.isEditable, false)
    }
    
    func testTextViewIsEditable() {
        // Arrange
        let editableState = PageSystem.State(
            pagePath: page.path,
            savedText: page.text,
            isEditing: true,
            currentText: nil
        )
        
        var viewOnlyState = editableState
        viewOnlyState.isEditing = false
        
        // Act
        sut.viewModelObserver.send(value: editableState)
        
        // Assert
        XCTAssertEqual(sut.textView.isEditable, true)
        
        // Rearrange
        sut.viewModelObserver.send(value: viewOnlyState)
        
        // Reassert
        XCTAssertEqual(sut.textView.isEditable, false)
    }
    
    func testBarButtonsWhenNotEditable() {
        let viewOnlyState = PageSystem.State(
            pagePath: page.path,
            savedText: page.text,
            isEditing: false,
            currentText: nil
        )
        
        sut.viewModelObserver.send(value: viewOnlyState)
        
        let leftBarButtonItems = sut.navigationItem.leftBarButtonItems
        XCTAssertEqual(leftBarButtonItems?.count, 1)
        let pageButton = leftBarButtonItems?.first
        XCTAssertEqual(pageButton?.title, "Pages")
        
        let rightBarButtonItems = sut.navigationItem.rightBarButtonItems
        XCTAssertEqual(rightBarButtonItems?.count, 1)
        let editButton = rightBarButtonItems?.first
        XCTAssertEqual(editButton?.title, "Edit")
        
        XCTAssertEqual(sut.navigationItem.hidesBackButton, false)
    }
    
    func testBarButtonsWhenEditable() {
        let editingState = PageSystem.State(
            pagePath: page.path,
            savedText: page.text,
            isEditing: true,
            currentText: nil
        )
        
        sut.viewModelObserver.send(value: editingState)
        
        let leftBarButtonItems = sut.navigationItem.leftBarButtonItems
        XCTAssertEqual(leftBarButtonItems?.count, 1)
        let cancelButton = leftBarButtonItems?.first
        XCTAssertEqual(cancelButton?.title, "Cancel")
        
        let rightBarButtonItems = sut.navigationItem.rightBarButtonItems
        XCTAssertEqual(rightBarButtonItems?.count, 1)
        let saveButton = rightBarButtonItems?.first
        XCTAssertEqual(saveButton?.title, "Save")
        
        XCTAssertEqual(sut.navigationItem.hidesBackButton, true)
    }
    
    // MARK: - Events
    
    func testTappingEditButtonSendsEvent() {
        let testObserver = TestObserver<PageSystem.Event, NoError>()
        sut.eventSignal.observe(testObserver.observer)
        
        let viewOnlyState = PageSystem.State(
            pagePath: page.path,
            savedText: page.text,
            isEditing: false,
            currentText: nil
        )
        
        sut.viewModelObserver.send(value: viewOnlyState)
        
        let editButton = sut.navigationItem.rightBarButtonItem!
        (editButton.target as! CocoaAction<UIBarButtonItem>).execute(editButton)

        XCTAssertEqual(testObserver.values, [PageSystem.Event.editButtonPressed])
    }
    
    func testTappingSaveButtonSendsEvent() {
        let testObserver = TestObserver<PageSystem.Event, NoError>()
        sut.eventSignal.observe(testObserver.observer)

        let editState = PageSystem.State(
            pagePath: page.path,
            savedText: page.text,
            isEditing: true,
            currentText: nil
        )
        
        sut.viewModelObserver.send(value: editState)
        
        let saveButton = sut.navigationItem.rightBarButtonItem!
        (saveButton.target as! CocoaAction<UIBarButtonItem>).execute(saveButton)
        
        XCTAssertEqual(testObserver.values, [PageSystem.Event.saveButtonPressed(page.text)])
    }
    
    func testTappingCancelButtonSendsEvent() {
        let testObserver = TestObserver<PageSystem.Event, NoError>()
        sut.eventSignal.observe(testObserver.observer)
        
        let editState = PageSystem.State(
            pagePath: page.path,
            savedText: page.text,
            isEditing: true,
            currentText: nil
        )
        
        sut.viewModelObserver.send(value: editState)
        
        let cancelButton = sut.navigationItem.leftBarButtonItem!
        (cancelButton.target as! CocoaAction<UIBarButtonItem>).execute(cancelButton)
        
        XCTAssertEqual(testObserver.values, [PageSystem.Event.cancelButtonPressed])
    }
}
