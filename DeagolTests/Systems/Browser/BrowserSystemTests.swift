//
//  BrowserSystemTests.swift
//  DeagolTests
//
//  Created by Matthew Lorentz on 3/19/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

@testable import Deagol
import XCTest
import CoreData
import ReactiveSwift
import ReactiveFeedback
import Result

class BrowserSystemTests: XCTestCase {
    
    var sut: BrowserSystem!
    
    var context: NSManagedObjectContext!

    var dataStore: DataStore!
    
    var testFiles: [File]!

    override func setUp() {
        dataStore = TestDataHelper.emptyInMemoryStore()
        context = dataStore.viewContext
        testFiles = SampleDataGenerator.createSampleFiles(in: context)
        try! context.save()
    }

    // MARK: - Reduce Tests

    func testReduceLoadData() {
        // Arrange
        let initialState = BrowserSystem.State(
            files: [],
            currentDirectory: nil,
            isLoadingFromDisk: false,
            isLoadingFromServer: false,
            directoryToShow: nil,
            pageToShow: nil
        )
        
        var expectedState = initialState
        expectedState.isLoadingFromDisk = true

        // Act
        let finalState = BrowserSystem.State.reduce(state: initialState, event: .loadDataFromDisk)
        
        // Assert
        XCTAssertEqual(finalState, expectedState)
    }
    
    func testReduceFinishedLoadingData() {
        // Arrange
        let initialState = BrowserSystem.State(
            files: [],
            currentDirectory: nil,
            isLoadingFromDisk: true,
            isLoadingFromServer: false,
            directoryToShow: nil,
            pageToShow: nil
        )
        
        var expectedState = initialState
        expectedState.files = testFiles
        expectedState.isLoadingFromDisk = false

        // Act
        let finalState = BrowserSystem.State.reduce(state: initialState, event: .dataWasLoadedFromDisk(testFiles))
        
        // Assert
        XCTAssertEqual(finalState, expectedState)
    }
    
    func testReduceRowTappedGivenNoFiles() {
        // Arrange
        let initialState = BrowserSystem.State(
            files: [],
            currentDirectory: nil,
            isLoadingFromDisk: true,
            isLoadingFromServer: false,
            directoryToShow: nil,
            pageToShow: nil
        )
        
        // Act
        let finalState = BrowserSystem.State.reduce(state: initialState, event: .rowTapped(0))
        
        // Assert
        XCTAssertEqual(finalState, initialState)
    }
    
    func testReduceRowTappedGivenDirectoryTapped() {
        // Arrange
        let directoryIndex = 2
        let directory = testFiles.last as! Directory
        
        let initialState = BrowserSystem.State(
            files: testFiles,
            currentDirectory: nil,
            isLoadingFromDisk: true,
            isLoadingFromServer: false,
            directoryToShow: nil,
            pageToShow: nil
        )
        
        var expectedState = initialState
        expectedState.directoryToShow = directory

        // Act
        let finalState = BrowserSystem.State.reduce(state: initialState, event: .rowTapped(directoryIndex))
        
        // Assert
        XCTAssertEqual(finalState, expectedState)
    }
    
    func testReduceRowTappedGivenRowTapped() {
        // Arrange
        let pageIndex = 0
        let page = testFiles.first as! Page

        let initialState = BrowserSystem.State(
            files: testFiles,
            currentDirectory: nil,
            isLoadingFromDisk: true,
            isLoadingFromServer: false,
            directoryToShow: nil,
            pageToShow: nil
        )
        
        var expectedState = initialState
        expectedState.pageToShow = page

        // Act
        let finalState = BrowserSystem.State.reduce(state: initialState, event: .rowTapped(pageIndex))
        
        // Assert
        XCTAssertEqual(finalState, expectedState)
    }
    
    func testReducePageWasShown() {
        // Arrange
        let page = testFiles.first as! Page
        
        let initialState = BrowserSystem.State(
            files: testFiles,
            currentDirectory: nil,
            isLoadingFromDisk: true,
            isLoadingFromServer: false,
            directoryToShow: nil,
            pageToShow: page
        )
        
        var expectedState = initialState
        expectedState.pageToShow = nil

        // Act
        let finalState = BrowserSystem.State.reduce(state: initialState, event: .pageWasShown)
        
        // Assert
        XCTAssertEqual(finalState, expectedState)
    }

    func testReduceFinishedShowingDirectoryDetail() {
        // Arrange
        let directory = testFiles.last as! Directory

        let initialState = BrowserSystem.State(
            files: testFiles,
            currentDirectory: nil,
            isLoadingFromDisk: true,
            isLoadingFromServer: false,
            directoryToShow: directory,
            pageToShow: nil
        )
        
        var expectedState = initialState
        expectedState.directoryToShow = nil

        // Act
        let finalState = BrowserSystem.State.reduce(state: initialState, event: .directoryWasShown)
        
        // Assert
        XCTAssertEqual(finalState, expectedState)
    }
    
    // MARK: - Feedback tests
    
    func testLoadDataFeedbackForRootDirectory() {
        // Arrange
        let feedback = BrowserSystem.loadDataFromDisk(context: context)
        let initialState = BrowserSystem.State.initial()
        var events = [BrowserSystem.Event]()
        
        let reduce = { (state: BrowserSystem.State, event: BrowserSystem.Event) -> BrowserSystem.State in
            events.append(event)
            return state
        }
        
        // Act
        let system = SignalProducer.system(initial: initialState, reduce: reduce, feedbacks: [feedback])
        system.start()

        // Assert
        eventually(XCTAssertEqual(events.count, 1))
        if case BrowserSystem.Event.dataWasLoadedFromDisk(let files) = events.first! {
            XCTAssertEqual(files.count, 2)
        }
    }
    
    func testLoadDataFeedbackForSubDirectory() {
        // Arrange
        let feedback = BrowserSystem.loadDataFromDisk(context: context)
        var initialState = BrowserSystem.State.initial()
        initialState.currentDirectory = testFiles.last as? Directory
        var events = [BrowserSystem.Event]()
        
        let reduce = { (state: BrowserSystem.State, event: BrowserSystem.Event) -> BrowserSystem.State in
            events.append(event)
            return state
        }
        
        // Act
        let system = SignalProducer.system(initial: initialState, reduce: reduce, feedbacks: [feedback])
        system.start()
        
        // Assert
        eventually(XCTAssertEqual(events.count, 1))
        if case BrowserSystem.Event.dataWasLoadedFromDisk(let files) = events.first! {
            XCTAssertEqual(files.count, 1)
        }
    }
    
    func testNavigationFeedback() {
        // Arrange
        let (_, appEventObserver) = Signal<AppSystem.Event, NoError>.pipe()
        let navigationController = UINavigationController()
        let feedback = BrowserSystem.navigation(appEventsObserver: appEventObserver, api: API(host: ""), dataStore: dataStore, navigationController: navigationController)
        let directory = testFiles.last as? Directory
        
        var initialState = BrowserSystem.State.initial()
        initialState.directoryToShow = directory
        
        var events = [BrowserSystem.Event]()
        
        let reduce = { (state: BrowserSystem.State, event: BrowserSystem.Event) -> BrowserSystem.State in
            events.append(event)
            return state
        }
        
        // Act
        let system = SignalProducer.system(initial: initialState, reduce: reduce, feedbacks: [feedback])
        system.start()
        
        // Assert
        eventually(XCTAssertEqual(navigationController.viewControllers.count, 1))
        let directoryBrowser = navigationController.viewControllers.first as! BrowserViewController
        XCTAssertEqual(directoryBrowser.state?.value.currentDirectory, directory)
        XCTAssertEqual(directoryBrowser.state?.value.directoryToShow, nil)
        XCTAssertEqual(directoryBrowser.state?.value.pageToShow, nil)
    }
    
    func testBindUIFeedback() {
        let (eventSignal, eventObserver) = Signal<BrowserSystem.Event, NoError>.pipe()
        let feedback = BrowserSystem.userInput(signal: eventSignal)
        let expectedPage = testFiles.first as! Page

        var initialState = BrowserSystem.State.initial()
        initialState.files = [expectedPage]
        initialState.isLoadingFromDisk = false
        
        var events = [BrowserSystem.Event]()
        
        let reduce = { (state: BrowserSystem.State, event: BrowserSystem.Event) -> BrowserSystem.State in
            events.append(event)
            return BrowserSystem.State.reduce(state: state, event: event)
        }
        
        let system = SignalProducer.system(initial: initialState, reduce: reduce, feedbacks: [feedback])
        let state = Property<BrowserSystem.State>(initial: initialState, then: system)
        
        // Act
        eventObserver.send(value: .rowTapped(0))

        // Assert
        XCTAssertEqual(events, [BrowserSystem.Event.rowTapped(0)])
        XCTAssertEqual(state.value.pageToShow, expectedPage)
    }
    
    // MARK: - IntegrationTests
    
    func testTappingRowTriggersAppSignal() {
        let pageToShow = testFiles.first as! Page
        let (eventSignal, eventObserver) = Signal<BrowserSystem.Event, NoError>.pipe()
        let navigationController = UINavigationController()
        let testObserver = TestObserver<AppSystem.Event, NoError>()
        
        let initialState = BrowserSystem.State(
            files: testFiles,
            currentDirectory: nil,
            isLoadingFromDisk: false,
            isLoadingFromServer: false,
            directoryToShow: nil,
            pageToShow: nil
        )
        
        let system = BrowserSystem(
            initialState: initialState,
            userInput: eventSignal,
            api: API(host: ""),
            dataStore: dataStore,
            navigationController: navigationController
        )
        
        system.appEvents.observe(testObserver.observer)
        
        eventObserver.send(value: .rowTapped(0))
        
        XCTAssertEqual(testObserver.values, [.showPageDetail(pageToShow)])
    }
    
    /// Verifies that the BrowserSystem's `appSignal` still emits .showPageDetail events after it
    /// has shown a sub-directory.
    /// Checks for a bug that was occuring in the `BrowserSystem.appEvents(observer:)` Feedback.
    func testTappingRowInSubDirectoryTriggersAppSignal() {
        // Arrange
        let pageToShow = testFiles.first as! Page
        let (eventSignal, eventObserver) = Signal<BrowserSystem.Event, NoError>.pipe()
        let navigationController = UINavigationController()
        let testObserver = TestObserver<AppSystem.Event, NoError>()
        
        let initialState = BrowserSystem.State(
            files: testFiles,
            currentDirectory: nil,
            isLoadingFromDisk: false,
            isLoadingFromServer: false,
            directoryToShow: nil,
            pageToShow: nil
        )
        
        let system = BrowserSystem(
            initialState: initialState,
            userInput: eventSignal,
            api: API(host: ""),
            dataStore: dataStore,
            navigationController: navigationController
        )
        
        system.appEvents.observe(testObserver.observer)
        
        // Act
        // Tap the directory to show a new Browser
        eventObserver.send(value: .rowTapped(2))
        
        // Tap a page in the original system
        eventObserver.send(value: .rowTapped(0))
        
        // Assert
        XCTAssertEqual(testObserver.values, [.showPageDetail(pageToShow)])
    }
}
