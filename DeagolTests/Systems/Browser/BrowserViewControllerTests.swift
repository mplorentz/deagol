//
//  BrowserViewControllerTests.swift
//  DeagolTests
//
//  Created by Matthew Lorentz on 3/14/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

@testable import Deagol
import XCTest
import ReactiveSwift
import Result

/// A suite of tests for the `PageBrowserViewController`.
class BrowserViewControllerTests: XCTestCase {
    
    /// System-under-test
    var sut: BrowserViewController!
    
    var testFiles: [File]!
    
    override func setUp() {
        // Set up view controller
        sut = BrowserViewControllerTests.browserViewController()
        let context = TestDataHelper.emptyInMemoryStore().viewContext
        testFiles = SampleDataGenerator.createSampleFiles(in: context)
        try! context.save()
        
        let defaultState = BrowserSystem.State(
            files: testFiles,
            currentDirectory: nil,
            isLoadingFromDisk: true,
            isLoadingFromServer: false,
            directoryToShow: nil,
            pageToShow: nil
        )

        // Initialize view model with sample data
        sut.state = Property<BrowserSystem.State>(value: defaultState)

        // Start view lifecycle
        _ = sut.view
    }
    
    /// Instantiates the browser view controller from a storyboard
    static func browserViewController() -> BrowserViewController {
        
        let nav = UINavigationController()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "Browser") as! BrowserViewController
        nav.viewControllers = [viewController]
        
        return viewController
    }

    // MARK: - Table View Content Tests
    
    /// Test that the table view has the correct number of rows.
    func testCorrectNumberOfRowsDisplayed() {
        XCTAssertEqual(sut.numberOfSections(in: sut.tableView), 1)
        XCTAssertEqual(sut.tableView(sut.tableView, numberOfRowsInSection: 0), 3)
    }
    
    /// Test that the table view rows representing directories have a disclosure indicator.
    func testDirectoryRowHasDisclosureIndicator() {
        let directoryCell = sut.tableView(sut.tableView, cellForRowAt: IndexPath(row: 2, section: 0))
        
        XCTAssertEqual(directoryCell.accessoryType, .disclosureIndicator)
    }
    
    /// Test that the table view rows representing pages do not have a disclosure indicator.
    func testPageRowDoesNotHaveDisclosureIndicator() {
        let pageCell = sut.tableView(sut.tableView, cellForRowAt: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(pageCell.accessoryType, .none)
    }
    
    // MARK: - Bindings

    /// Tests that the view controller has the correct title.
    func testTitle() {
        // Arrange
        sut = BrowserViewControllerTests.browserViewController()
        let (stateSignal, stateObserver) = Signal<BrowserSystem.State, NoError>.pipe()
        sut.state = Property<BrowserSystem.State>(initial: BrowserSystem.State.initial(), then: stateSignal)
        sut.viewDidLoad()
        
        // Act
        stateObserver.send(value: BrowserSystem.State.initial())
        
        // Assert
        XCTAssertEqual(sut.title, "Pages")
        XCTAssertEqual(sut.navigationItem.title, "Pages")
        
        // Rearrange
        let directory = testFiles[2] as! Directory
        var newState = BrowserSystem.State.initial()
        newState.currentDirectory = directory

        // React
        stateObserver.send(value: newState)
        
        XCTAssertEqual(sut.title, "sub directory")
        XCTAssertEqual(sut.navigationItem.title, "sub directory")
    }
    
    /// Verifies that when the view controller's state changes the table view reloads
    func testTableViewReloadsWhenStateChanges() {
        // Arrange
        sut = BrowserViewControllerTests.browserViewController()
        
        let mockTableView = MockTableView()
        sut.tableView = mockTableView
        let (stateSignal, stateObserver) = Signal<BrowserSystem.State, NoError>.pipe()
        sut.state = Property<BrowserSystem.State>(initial: BrowserSystem.State.initial(), then: stateSignal)
        sut.viewDidLoad()

        // Act
        stateObserver.send(value: BrowserSystem.State.initial())

        // Assert
        XCTAssertEqual(mockTableView.reloadDataCallCount, 1)
    }
    
    /// Tests that the `clearsSelectionOnViewWillAppear` property is properly bound.
    /// Disabled because I can't figure out how to change the `isCollapsed` property on a `UISplitViewContoller`.
    func disabledTestCollapseWhenInSplitView() {
        // Arrange
        let splitView = UISplitViewController()
        let detailViewController = UIViewController()
        sut = BrowserViewControllerTests.browserViewController()
        splitView.viewControllers = [sut, detailViewController]
        sut.viewDidLoad()
        

        // Act
//        splitView.willTransition(to: UITraitCollection.init(horizontalSizeClass: .regular), with: UIViewControllerTransitionCoordinator())
        
        // Assert
        XCTAssertEqual(sut.clearsSelectionOnViewWillAppear, true)
        
        // Rearrange

        // Reassert
        XCTAssertEqual(sut.clearsSelectionOnViewWillAppear, false)
    }

    // MARK: - Events
    
    /// Tests that tapping a row in the table triggers its view model's tapped signal.
    func testTappingRowSignalsEvent() {
        let testObserver = TestObserver<BrowserSystem.Event, NoError>()
        sut.eventSignal.observe(testObserver.observer!)

        sut.tableView(sut.tableView, didSelectRowAt: IndexPath(row: 1, section: 0))
        
        XCTAssertEqual(testObserver.values.count, 1)
        XCTAssertEqual(testObserver.values.first, .rowTapped(1))
    }
    
    /// Test that the UIRefreshControl emits the correct event.
    func testPullToRefreshEmitsEvent() {
        // Arrange
        let testObserver = TestObserver<BrowserSystem.Event, NoError>()
        sut.eventSignal.observe(testObserver.observer!)
        
        // Act
        sut.tableView.refreshControl?.sendActions(for: UIControl.Event.valueChanged)
        
        // Assert
        XCTAssertEqual(testObserver.values, [.loadDataFromServer])
    }
    
    /// Tests that the UIRefreshControl animation is properly bound to isLoadingFromServer.
    func testPullToRefreshAnimates() {
        // Arrange
        sut = BrowserViewControllerTests.browserViewController()
        let (stateSignal, stateObserver) = Signal<BrowserSystem.State, NoError>.pipe()
        sut.state = Property<BrowserSystem.State>(initial: BrowserSystem.State.initial(), then: stateSignal)
        sut.viewDidLoad()
        
        var loadingState = BrowserSystem.State.initial()
        loadingState.isLoadingFromServer = true
        
        var notLoadingState = BrowserSystem.State.initial()
        notLoadingState.isLoadingFromServer = false

        // Act
        stateObserver.send(value: loadingState)

        // Assert
        XCTAssertEqual(sut.tableView.refreshControl?.isRefreshing, true)
        
        // Rearrange
        stateObserver.send(value: notLoadingState)

        // React
        XCTAssertEqual(sut.tableView.refreshControl?.isRefreshing, false)
    }
}
