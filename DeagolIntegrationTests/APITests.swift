//
//  APITests.swift
//  DeagolTests
//
//  Created by Matthew Lorentz on 3/22/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

@testable import Deagol
import XCTest
import SwiftSoup
import CoreData
import ReactiveSwift
import Result

/// Some weak tests to see if the API plumbing is working.
class APITests: XCTestCase {
    
    var dataStore: DataStore!
    
    var api: API!
    
    var context: NSManagedObjectContext!
    
    let subPageContents = "this is the sub page"
    
    override func setUp() {
        dataStore = TestDataHelper.emptyInMemoryStore()
        context = dataStore.viewContext
        api = API(host: "http://157.230.53.153:4567")
    }

    func testFilesList() throws {
        let result = api.downloadFiles(into: context).collect().last()!
        let rootFiles = try DataStore.files(context: context)
        switch result {
        case .success(let files):
            XCTAssert(files.count >= rootFiles.count)
        case .failure(let error):
            XCTFail(error.localizedDescription)
        }
        

        XCTAssertEqual(rootFiles[2].name, "Home")
        XCTAssertEqual(rootFiles[3].name, "Index")
        XCTAssertEqual(rootFiles[5].name, "sub-directory")

        let directory = rootFiles[5] as! Directory
        let subPage = Array(directory.children!).first as! Page
        XCTAssertEqual(subPage.text, subPageContents)
        XCTAssertEqual(directory.children!.count, 1)
        XCTAssertEqual((Array(directory.children!).first as? Page)?.name, "sub page")
    }

    func testDownloadTextForPage() {
        let subDirectory = Directory(context: context)
        subDirectory.name = "sub directory"
        
        let subPage = Page(context: context)
        subPage.name = "sub page"
        subPage.parentDirectory = subDirectory


        let result = api.downloadText(for: subPage).last()!
        switch result {
        case .success(let file):
            XCTAssertEqual((file as! Page).text, subPageContents)
        case .failure(let error):
            XCTFail(error.localizedDescription)
        }
    }
    
    func testUpdateHomePage() {
        let page = Page(context: context)
        page.name = "Home"
        
        let newText = "This is the new text. posted \(Date())"
        
        let result = api.updatePage(at: page.path, with: newText).last()!
        
        switch result {
        case .success(let text):
            XCTAssertEqual(text, newText)
        case .failure(let error):
            XCTFail(error.localizedDescription)
        }
    }
}
