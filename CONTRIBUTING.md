# Project Setup

1. `git clone git@gitlab.com:mplorentz/deagol.git`
2. `open deagol/Deagol.xcworkspace`
3. Build and Run

## Optional Dependencies

* `brew install adr-tools` for working with the architecture design records stored in doc/adr.
* `gem install cocoapods` if you need add or update dependencies.
