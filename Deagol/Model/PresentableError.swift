//
//  PresentableError.swift
//  Deagol
//
//  Created by Matthew Lorentz on 3/13/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

import Foundation

/// An error with a message that can be presented to the user.
struct PresentableError: Error, Equatable {
    
    /// The error message. Should be something that can be displayed in the UI.
    var message: String
}
