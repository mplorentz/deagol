//
//  DataStore.swift
//  Deagol
//
//  Created by Matthew Lorentz on 3/23/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

import CoreData
import ReactiveSwift
import Result

/// A persistent container for our models that is stored on-device.
class DataStore: NSPersistentContainer {
    
    /// A singleton objectModel because Core Data gets cranky if you instantiate
    /// more than one.
    fileprivate static var objectModel: NSManagedObjectModel = {
        guard let modelURL = Bundle.main.url(forResource: "Deagol", withExtension: "momd") else {
            fatalError("Unable to Find Data Model")
        }
        
        guard let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Unable to Load Data Model")
        }
        
        return managedObjectModel
    }()
    
    // MARK: - Init
    
    /// Initializes the datastore.
    ///
    /// - parameter inMemory: If true the datastore will store objects in
    ///     memory and they will not be saved after the app has closed. By
    ///     default objects will be stored on disk.
    init(inMemory: Bool = false) {

        super.init(name: "Deagol", managedObjectModel: DataStore.objectModel)

        if inMemory {
            let description = NSPersistentStoreDescription()
            description.type = NSInMemoryStoreType
            description.shouldAddStoreAsynchronously = false
            persistentStoreDescriptions = [description]
        }
        
        loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
    }
    
    // MARK: - Fetching objects
    
    /// Tries to find the default page to be shown on app launch.
    func initialPage(in context: NSManagedObjectContext? = nil) -> Page? {
        let context = context ?? viewContext
        
        // Fetch all pages in the root directory
        let request = NSFetchRequest<Page>(entityName: "Page")
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        request.predicate = NSPredicate(format: "parentDirectory = nil")
        
        let rootPages: [Page] = (try? context.fetch(request)) ?? []
        
        // Look for a page named Home
        if let homePage = rootPages.filter({ $0.name?.lowercased() == "home" }).first {
            return homePage
        }
        
        // Look for a page named index
        if let indexPage = rootPages.filter({ $0.name?.lowercased() == "index" }).first {
            return indexPage
        }
        
        return rootPages.first
    }
    
    /// Finds the page with the given path.
    ///
    /// - parameter path: The absolute path to the page you wish to fetch.
    /// - parameter context: The context in which to look for the page. Defaults to the `viewContext`.
    func page(at path: String, in context: NSManagedObjectContext? = nil) -> Page? {
        return file(at: path, in: context) as? Page
    }
    
    /// Finds the file with the given path.
    ///
    /// - parameter path: The absolute path to the file you wish to fetch.
    /// - parameter context: The context in which to look for the file. Defaults to the `viewContext`.
    func file(at path: String, in context: NSManagedObjectContext? = nil) -> File? {
        let context = context ?? viewContext
        
        // Walk through the path components until we get to the destination page.
        return path.split(separator: "/").reduce(nil) { (file: File?, nextPathComponent) -> File? in
            let fileName = String(nextPathComponent)
            
            // First step: look for a file with the given name in the root directory.
            if file == nil {
                let request = NSFetchRequest<File>(entityName: "File")
                request.predicate = NSPredicate(format: "parentDirectory = nil && name = %@", fileName)
                let rootFiles: [File]? = try? context.fetch(request)
                return rootFiles?.first
                
            // If this is a directory look through its children for the next file.
            } else if let directory = file as? Directory,
                let childrenSet = directory.children,
                let children = Array(childrenSet) as? [File] {
                return Array(children).first(where: { $0.name == fileName })
            } else {
                
                // File could not be found.
                return nil
            }
        }
    }
    
    /// Fetches all the files in the given context.
    static func allFiles(in context: NSManagedObjectContext) throws -> [File] {
        let request = NSFetchRequest<File>(entityName: "File")
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        return try context.fetch(request)
    }

    /// Fetches all the files in the given directory and context. Files will be sorted alphabetically.
    /// If no directory is given then only files from the root directory will be returned.
    static func files(inDirectory directory: Directory? = nil, context: NSManagedObjectContext) throws -> [File] {
        
        let fetchRequest = NSFetchRequest<File>(entityName: "File")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSString.caseInsensitiveCompare))]
        
        if let directory = directory {
            fetchRequest.predicate = NSPredicate(format: "parentDirectory = %@", directory)
        } else {
            fetchRequest.predicate = NSPredicate(format: "parentDirectory = nil")
        }
        
        return try context.fetch(fetchRequest)
    }
    
    /// Deletes all file in the given context.
    static func deleteAllFiles(in context: NSManagedObjectContext) throws {
        let request = NSFetchRequest<File>(entityName: "File")
        let oldFiles: [File] = try context.fetch(request)
        oldFiles.forEach { context.delete($0) }
    }
}
