//
//  File.swift
//  Deagol
//
//  Created by Matthew Lorentz on 3/15/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

import Foundation

extension File {
    
    var path: String {
        if let parent = parentDirectory {
            return "\(parent.path)/\(name!)"
        } else {
            return name!
        }
    }
}
