//
//  Directory.swift
//  Deagol
//
//  Created by Matthew Lorentz on 3/29/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

import CoreData
import SwiftSoup

extension Directory {
    
    convenience init(from soup: SwiftSoup.Element, parentDirectory: Directory?, context: NSManagedObjectContext) throws {
        
        do {
            let name = try soup.child(0).text()
            
            self.init(context: context)
            
            self.name = name
            self.parentDirectory = parentDirectory
            
        } catch {
            throw PresentableError(message: "Error parsing page")
        }
    }
}
