//
//  NSPersistentContainer+isEmpty.swift
//  Deagol
//
//  Created by Matthew Lorentz on 3/13/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

import CoreData

extension NSPersistentContainer {
    
    /// Takes a container and returns true if it contains no models and false otherwise.
    func isEmpty() throws -> Bool {
        let directoryRequest = NSFetchRequest<Directory>(entityName: "Directory")
        let directories = try viewContext.fetch(directoryRequest)
        
        
        let pageRequest = NSFetchRequest<Page>(entityName: "Page")
        let pages = try viewContext.fetch(pageRequest)
        
        return pages.isEmpty && directories.isEmpty
    }
}
