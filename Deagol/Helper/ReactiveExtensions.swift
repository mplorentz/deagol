//
//  ReactiveExtensions.swift
//  Deagol
//
//  Created by Matthew Lorentz on 3/13/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

import ReactiveSwift

// https://github.com/kickstarter/Kickstarter-ReactiveExtensions/blob/master/ReactiveExtensions/operators/IgnoreValues.swift

public extension Signal {
    
    /**
     Creates a new signal that emits a void value for every emission of `self`.
     - returns: A new signal.
     */
    public func ignoreValues() -> Signal<Void, Error> {
        return signal.map { _ in () }
    }
}

public extension SignalProducer {
    
    /**
     Creates a new producer that emits a void value for every emission of `self`.
     - returns: A new producer.
     */
    public func ignoreValues() -> SignalProducer<Void, Error> {
        return lift { $0.ignoreValues() }
    }
}
