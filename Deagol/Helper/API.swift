//
//  API.swift
//  Deagol
//
//  Created by Matthew Lorentz on 3/22/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

import Foundation
import ReactiveSwift
import ReactiveCocoa
import CoreData
import Result
import SwiftSoup

struct API {
    
    /// An protocol + IP adress to the host of this interface.
    var host: String
    
    // MARK: - Public interface
    
    /// Fetches all wiki files from the `host` and saves them into the given `context`. Returns
    /// a `SignalProducer` that emits each files as it is downloaded and a `completed` signal
    /// when all files have been downloaded.
    ///
    /// Please note that any existing files in the given `context` will be deleted and replaced with fresh data.
    /// This producer will use a background queue. Remember to observe on the main queue if necessary.
    ///
    /// - parameter context: The object context that the files will be inserted into.
    /// - returns: A `SignalProducer` that will emit `File`s as they are downloaded.
    public func downloadFiles(into context: NSManagedObjectContext) -> SignalProducer<File, PresentableError> {
        
        let endpoint = "fileview"
        
        guard let url = URL(string: "\(self.host)/\(endpoint)") else {
            return SignalProducer(error: PresentableError(message: "Internal error"))
        }
        
        return URLSession.shared.reactive
            .data(with: URLRequest(url: url))
            .observe(on: QueueScheduler())
            .mapError { (_) -> PresentableError in
                PresentableError(message: "Internal Error")
            }
            .flatMap(.merge) {
                (response: (Data, URLResponse)) -> SignalProducer<File, PresentableError> in
                
                self.handleFileList(response: response, context: context)
            }
    }
    
    /// Tells the server to update the page at at the given path with the new text.
    ///
    /// - parameter path: An absolute path to the page you wish to update. Does not need to be URL-encoded.
    /// - parameter newText: The contents of the page.
    /// - returns: A producer that will emit the `newText` parameter when the request has finished and then complete.
    public func updatePage(at path: String, with newText: String) -> SignalProducer<String, AnyError> {
        
        let pathComponents = path.split(separator: "/")
        var pageName: String
        if pathComponents.count > 1 {
            pageName = String(pathComponents[pathComponents.count - 1])
        } else {
            pageName = path
        }

        guard let encodedPath = path.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed),
            let url = URL(string: "\(self.host)/edit/\(encodedPath)") else {
                return SignalProducer<String, AnyError>(error: AnyError(PresentableError(message: "Internal error")))
        }
        
        let pathWithoutName = String(path.prefix(path.count - pageName.count))
        
        let parameters: [String : Any] = [
            "page": pageName,
            "path": pathWithoutName,
            "format": "markdown",
            "content": newText,
            "message": "Edit from an anonymous Deagol user."
        ]
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = parameters.percentEscaped().data(using: .utf8)

        return URLSession.shared.reactive
            .data(with: request)
            .map { (_: (Data, URLResponse)) -> String in
                return newText
        }
    }
    
    // MARK: - Helpers
    
    /// A helper that takes HTTP response data from the /fileview endpoing and parses the `File`s
    /// contained in it.
    ///
    /// This function will fire off several more web requests to fetch the contents of each file.
    /// Please note that any existing files in the given `context` will be deleted and replaced with fresh data.
    ///
    /// - parameter response: A tuple containing HTML data from the /fileview endpoint and an associated `URLResponse`).
    /// - parameter context: The object context that the files will be inserted into.
    /// - returns: A `SignalProducer` that will emit each `File` as it is downloaded.
    internal func handleFileList(response: (Data, URLResponse), context: NSManagedObjectContext) -> SignalProducer<File, PresentableError> {
        
        let data = response.0
        let parseError = SignalProducer<File, PresentableError>(
            error: PresentableError(message: "Could not parse server response.")
        )
        
        /// Convert Data to an HTML String
        guard let html = String(data: data, encoding: .utf8) else {
            return parseError
        }
        
        // Map HTML String to Soup
        var listSoup: SwiftSoup.Element
        do {
            let doc: Document = try SwiftSoup.parse(html)
            let maybeSoup = try doc.getElementById("results")?.child(0)
            
            if let maybeSoup = maybeSoup {
                listSoup = maybeSoup
            } else {
                return parseError
            }
        } catch {
            return parseError
        }

        // Clear out old files instead of trying to merge them with new ones.
        do {
            try DataStore.deleteAllFiles(in: context)
        } catch {
            return SignalProducer(error: PresentableError(message: error.localizedDescription))
        }
        
        // Parse and fetch page contents

        return parseFiles(from: listSoup, context: context)
            .flatMap(FlattenStrategy.concurrent(limit: 4)) {
                (file: File) -> SignalProducer<File, PresentableError> in
                self.downloadText(for: file)
        }
    }
    
    /// Fetches the contents for the given file. If it is a directory nothing will be downloaded.
    ///
    /// - parameter file: The file whose text you wish to fetch.
    /// - returns: A producer that will emit the given file when the process has completed.
    internal func downloadText(for file: File) -> SignalProducer<File, PresentableError> {
        
        // If this is a directory just return
        guard let page = file as? Page else {
            return SignalProducer(value: file)
        }
        
        /// Prepare download URL
        guard let filePath = page.path.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed),
            let url = URL(string: "\(self.host)/data/\(filePath)") else {
                return SignalProducer(error: PresentableError(message: "Internal error"))
        }
        
        return URLSession.shared.reactive
            .data(with: URLRequest(url: url))
            .map { (data: Data, response: URLResponse) in
                page.text = String(decoding: data, as: UTF8.self)
                return page
            }
            .flatMapError { _ in
                return SignalProducer(error: PresentableError(message: "Error fetching page text"))
            }
    }

    /// This takes an HTML list of files and transforms them into Core Data entities.
    ///
    /// Please note that the parsed Pages will not have any `text`. See `downloadText(for:)` for that.
    ///
    /// - parameter soup: The parsed HTML list of files.
    /// - parameter directory: The parent directory of the files in the `soup`, if any.
    /// - parameter context: The context that the models will be created in.
    /// - returns: A producer that will emit all the File objects as they are parsed.
    internal func parseFiles(from soup: SwiftSoup.Element, in directory: Directory? = nil, context: NSManagedObjectContext) -> SignalProducer<File, PresentableError> {
        
        do {
            let fileElements = soup.children().filter { $0.hasClass("file") }
            let directoryElements = try soup.children().filter({ try $0.classNames().count == 0 })
        
            // Parse files
            let files = try fileElements.map {
                try Page(from: $0, parentDirectory: directory, context: context)
            }

            // Parse directories
            let directoryProducers = try directoryElements.map {
                (directoryElement: SwiftSoup.Element) -> SignalProducer<File, PresentableError> in

                let newDirectory = try Directory(from: directoryElement, parentDirectory: directory, context: context)

                // Recursively parse this directory's children
                let children = directoryElement.child(2)
                
                return SignalProducer(value: newDirectory)
                    .merge(with: self.parseFiles(from: children, in: newDirectory, context: context)
                )
            }
            
            let fileProducer = SignalProducer<File, PresentableError>(files)
            let directoryProducer = SignalProducer.merge(directoryProducers)
            
            return fileProducer.merge(with: directoryProducer)

        } catch {
            return SignalProducer(error: PresentableError(message: error.localizedDescription))
        }

    }
}
