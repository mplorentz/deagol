//
//  Dictionary+percentEscaped.swift
//  Deagol
//
//  Created by Matthew Lorentz on 3/29/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

import Foundation

extension Dictionary {
    func percentEscaped() -> String {
        return map { (key, value) in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
            }
            .joined(separator: "&")
    }
}
