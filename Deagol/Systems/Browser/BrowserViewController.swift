//
//  BrowserViewController.swift
//  Deagol
//
//  Created by Matthew Lorentz on 3/12/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa
import Result

/// A view that allows the user to browse the page structure of the wiki.
class BrowserViewController: UITableViewController {

    /// A reference to the current state of the BrowserSystem.
    /// I wish this could be a Signal instead of a Property, but since I haven't set up a reactive
    /// UITableViewDataSource this class needs to be able to access the current row data on-demand.
    var state : Property<BrowserSystem.State>?

    /// A pipe for events that may be emitted from this view.
    let (eventSignal, eventObserver) = Signal<BrowserSystem.Event, NoError>.pipe()

    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.refreshControl = UIRefreshControl()
        
        bindUI()
        
        eventObserver.send(value: .viewDidLoad)
    }
    
    deinit {
        eventObserver.sendCompleted()
        tableView.refreshControl?.reactive.refresh = nil
    }
    
    // MARK: - Helpers
    
    /// Binds the UI elements in this view to the `state`.
    func bindUI() {
        
        // Bind UI
        guard let signal = state?.signal else {
            return
        }

        // Reload table view when state changes
        // Not very efficient but I don't want to build a reactive data source right now.
        tableView.reactive.reloadData <~ signal.ignoreValues()
        
        // Title
        reactive.title <~ signal.map { $0.currentDirectory?.name ?? "Pages" }

        // Clear selection in this viewController when the split view controller is collapsed.
        _ = splitViewController?.reactive.signal(forKeyPath: "isCollapsed").observeValues { [weak self] in
            guard let isCollapsed = $0 as? Bool else { return }
            self?.clearsSelectionOnViewWillAppear = isCollapsed
        }

        // Bind pull-to-refresh
        tableView.refreshControl?.reactive.isRefreshing <~ signal.map { $0.isLoadingFromServer }
        tableView.refreshControl?.reactive.refresh = CocoaAction(Action<Void, Void, NoError>() {
            [weak self] _ in
            
            // Signal the need for new data
            self?.eventObserver.send(value: .loadDataFromServer)

            // Signal that we have finished refreshing when isLoadingFromDisk goes back to false.
            let refreshingSignal = signal.filter { $0.isLoadingFromDisk == false }.take(first: 1)
            return SignalProducer(refreshingSignal).ignoreValues()
        })
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return state?.value.files.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        if let file = state?.value.files[indexPath.row] {
            cell.textLabel?.text = file.name
            cell.accessoryType = file is Directory ? .disclosureIndicator : .none
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        eventObserver.send(value: .rowTapped(indexPath.row))
    }
}

