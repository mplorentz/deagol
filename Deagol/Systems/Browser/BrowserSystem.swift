//
//  BrowserSystem.swift
//  Deagol
//
//  Created by Matthew Lorentz on 3/18/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

import CoreData
import ReactiveSwift
import ReactiveFeedback
import Result
import CRNotifications

/// A system that facilitates browsing the page structure of the wiki.
class BrowserSystem {

    // MARK: - Public Interface
    
    /// A signal that sends `AppEvents`. These events are observed and handled by the `AppSystem`.
    var appEvents: Signal<AppSystem.Event, NoError>
    
    /// A property that holds the current state of the system.
    var state: Property<State>!
    
    /// A dispose bag that will be emptied just before this system's `state` is deallocated.
    var disposeBag = CompositeDisposable()
    
    // MARK: - Events
    
    /// An enumeration of the events that can occur in a BrowserSystem.
    enum Event: Equatable {
        
        /// Triggered when a row is tapped. Includes the index of the row that was tapped.
        case rowTapped(Int)
        
        /// Triggered when fresh data should be loaded from CoreData.
        case loadDataFromDisk
        
        /// Triggered when fresh data has been loaded from CoreData. Includes the new data.
        case dataWasLoadedFromDisk([File])
        
        /// Triggered when fresh data should be downloaded from the server.
        case loadDataFromServer
        
        /// Triggered when fresh data has been downloaded from the server.
        case dataWasLoadedFromServer
        
        /// Triggered when the system has finished displaying the `directoryToShow`.
        case directoryWasShown
        
        /// Triggered when the system has finished displaying the `pageToShow`.
        case pageWasShown
        
        /// Triggered when the view has finished loading.
        case viewDidLoad
        
        /// Triggered when an error occurs.
        case error(PresentableError)
    }
    
    // MARK: - Event
    
    /// A representation of the state of this system.
    struct State: Equatable {
        
        /// The files that this system should allow the user to browse.
        /// Each instance of `BrowserSystem` only allows the user to browse a single level files in the directory hierarchy.
        var files: [File]
        
        /// The current parent directory of the `files`. Will be nil if we are showing the root directory.
        var currentDirectory: Directory?
        
        /// Whether or not we are currently loading fresh data from Core Data.
        var isLoadingFromDisk = false
        
        /// Whether or not we are currently downloading fresh data from the server.
        var isLoadingFromServer = false
        
        /// A directory whose content should be shown to the user immediately.
        var directoryToShow: Directory?
        
        /// A page whose content should be shown to the user immediately.
        var pageToShow: Page?

        /// Computes new state from the given state and an event.
        static func reduce(state: State, event: Event) -> State {
            
            var newState = state
            
            switch event {
                
            case .loadDataFromDisk:
                newState.isLoadingFromDisk = true
                
            case .dataWasLoadedFromDisk(let files):
                newState.files = files
                newState.isLoadingFromDisk = false
            
            case .loadDataFromServer:
                newState.isLoadingFromServer = true

            case .dataWasLoadedFromServer:
                newState.isLoadingFromServer = false
                newState.isLoadingFromDisk = true

            case .rowTapped(let row) where state.files.count > row && state.files[row] is Directory:
                newState.directoryToShow = state.files[row] as? Directory
                
            case .rowTapped(let row) where state.files.count > row && state.files[row] is Page:
                newState.pageToShow = state.files[row] as? Page
                
            case .pageWasShown:
                newState.pageToShow = nil
                
            case .directoryWasShown:
                newState.directoryToShow = nil
                
            case .viewDidLoad:
                break
                
            case .error(let error):
                newState.isLoadingFromServer = false
                CRNotifications.showNotification(type: CRNotifications.error, title: "Error", message: error.message, dismissDelay: 3)

            default:
                break
            }
            
            return newState
        }
        
        /// The state that a `BrowserSystem` shoudl generally start with.
        static func initial() -> State {
            return State(
                files: [],
                currentDirectory: nil,
                isLoadingFromDisk: true,
                isLoadingFromServer: true,
                directoryToShow: nil,
                pageToShow: nil
            )
        }
    }

    
    // MARK: - Private Properties
    
    /// A pipe that allows us to forward `AppSystem.Event`s from feedback to the public `appEvents` signal.
    fileprivate var (_appEventsSignal, _appEventsObserver) = Signal<AppSystem.Event, NoError>.pipe()

    // MARK: - Init
    
    /// Initializes the system.
    ///
    /// - parameter initialState: The state that the system will start in.
    /// - parameter userInput: A signal of events generated by user input.
    /// - parameter api: An interface to the server that data will be downloaded from.
    /// - parameter dataStore: A dataStore that files will be loaded from.
    init(initialState: State = State.initial(),
         userInput: Signal<Event, NoError>,
         api: API,
         dataStore: DataStore,
         navigationController: UINavigationController
    ) {
        
        /// Assign the public `appEvents` signal to the private pipe.
        appEvents = _appEventsSignal

        let feedbacks = [
            BrowserSystem.loadDataFromDisk(context: dataStore.viewContext),
            BrowserSystem.loadDataFrom(api: api, context: dataStore.viewContext),
            BrowserSystem.userInput(signal: userInput),
            BrowserSystem.appEvents(observer: _appEventsObserver),
            BrowserSystem.navigation(
                appEventsObserver: _appEventsObserver,
                api: api,
                dataStore: dataStore,
                navigationController: navigationController
            )
        ]

        let producer = SignalProducer.system(
            initial: initialState,
            reduce: State.reduce,
            feedbacks: feedbacks
        )
        /// Dispose the bag on completion.
        .on(terminated: { [disposeBag] in
            disposeBag.dispose()
        })
        
        state = Property<State>(initial: initialState, then: producer)
    }

    // MARK: - Feedback
    
    /// Loads data from disk when necessary. Emits a dataWasLoadedFromDisk event with the new data
    /// when new data has been saved.
    ///
    /// - parameter context: The object context that data will be fetched from.
    /// - returns: The feedback.
    static func loadDataFromDisk(context: NSManagedObjectContext) -> Feedback<State, Event> {
        
        return Feedback<State, Event>(predicate: { $0.isLoadingFromDisk }) {
            (state: State) -> SignalProducer<Event, NoError> in
            
            do {
                let files = try DataStore.files(inDirectory: state.currentDirectory, context: context)
                return SignalProducer(value: .dataWasLoadedFromDisk(files))
            } catch {
                let error = PresentableError(message: "Could not read from the local database.")
                return SignalProducer(value: .error(error))
            }
        }
    }
    
    /// Loads data from the server when necessary. Emits a dataWasLoadedEventFromServer event when
    /// new data has been saved to the given context.
    ///
    /// - parameter context: The object context that new data will be saved to.
    /// - parameter api: The interface to the server that data should be downloaded from.
    /// - returns: The feedback.
    static func loadDataFrom(api: API, context: NSManagedObjectContext) -> Feedback<State, Event> {
        
        return Feedback<State, Event>(predicate: { $0.isLoadingFromServer }) {
            (state: State) -> SignalProducer<Event, NoError> in

            /// Save objects into a child context so the parent maintains a cohesive view
            /// of the world while new objects are being added and old ones deleted.
            let childContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            childContext.parent = context
            
            return api.downloadFiles(into: childContext)
                .collect()
                .observe(on: QueueScheduler.main)
                .attemptMap { (files: [File]) -> Result<Event, PresentableError> in
                    do {
                        try childContext.save()
                        try context.save()
                        return .success(Event.dataWasLoadedFromServer)
                    } catch {
                        return .failure(
                            PresentableError(
                                message: "Could not save data downloaded from the server"
                            )
                        )
                    }
                }
                .flatMapError({ (error: PresentableError) -> SignalProducer<Event, NoError> in
                    return SignalProducer(value: Event.error(error))
                })
        }
    }
    
    /// Transitions to new BrowserViewController when the user taps on a directory.
    ///
    /// - parameter appEventsObserver: A pipe to the public `appEvents` signal of the root
    ///     BrowserSystem. We pass this down so nested BrowserSystems can propagate an App Event
    ///     all the way up to the AppSystem.
    /// - parameter api: An interface to the server that data will be downloaded from.
    /// - parameter dataStore: A dataStore that files will be loaded from.
    /// - parameter navigationController: A controller that can will have new BrowserViewControllers
    ///     pushed onto it.
    /// - returns: The feedback.
    static func navigation(
        appEventsObserver: Signal<AppSystem.Event, NoError>.Observer,
        api: API,
        dataStore: DataStore,
        navigationController: UINavigationController
        ) -> Feedback<State, Event> {
        
        return Feedback<State, Event>(predicate: { $0.directoryToShow != nil }) {
            (state: State) -> SignalProducer<Event, NoError> in
            
            guard let directoryToShow = state.directoryToShow,
                let directoryBrowser = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Browser") as? BrowserViewController
            else {
                let error = PresentableError(message: "Internal Error.")
                return SignalProducer(value: Event.error(error))
            }
            
            // Instantiate the new system
            let initialState = State(
                files: [],
                currentDirectory: directoryToShow,
                isLoadingFromDisk: true,
                isLoadingFromServer: false,
                directoryToShow: nil,
                pageToShow: nil
            )
            
            let subSystem = BrowserSystem(
                initialState: initialState,
                userInput: directoryBrowser.eventSignal,
                api: api,
                dataStore: dataStore,
                navigationController: navigationController
            )
            
            directoryBrowser.state = subSystem.state
            
            // Show the new view controller
            navigationController.pushViewController(directoryBrowser, animated: true)
            
            // Bind the appEvents signal of the subsystem to the appEvents signal of the parent system.
            let disposable = subSystem.appEvents.observe(appEventsObserver)
            subSystem.disposeBag.add(disposable)

            return SignalProducer(value: .directoryWasShown)
        }
    }
    
    /// A feedback that passes events generated by user input into this system.
    static func userInput(signal eventSignal: Signal<Event, NoError>) -> Feedback<State, Event> {
        
        return Feedback<State, Event> { (scheduler, stateSignal) -> Signal<Event, NoError> in
            
            return eventSignal
        }
    }
    
    /// A feedback that notifies the AppSystem when it is time to display a new wiki page.
    static func appEvents(observer: Signal<AppSystem.Event, NoError>.Observer) -> Feedback<State, Event> {
        
        return Feedback(predicate: { $0.pageToShow != nil }) {
            (state) -> SignalProducer<Event, NoError> in
            
            guard let pageToShow = state.pageToShow else {
                return SignalProducer(value: Event.error(PresentableError(message: "Internal Error")))
            }
            
            observer.send(value: .showPageDetail(pageToShow))
            
            return SignalProducer(value: Event.pageWasShown)
        }
    }
}
