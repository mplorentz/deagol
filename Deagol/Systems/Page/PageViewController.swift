//
//  PageViewController.swift
//  Deagol
//
//  Created by Matthew Lorentz on 3/12/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa
import ReactiveFeedback
import Result

/// A view that allows the user to view and edit a wiki page.
class PageViewController: UIViewController {

    /// A pipe for the state of the PageSystem
    var (viewModelSignal, viewModelObserver) = Signal<PageSystem.State, NoError>.pipe()
    
    /// A pipe for events that may be emitted from this view.
    var (eventSignal, eventObserver) = Signal<PageSystem.Event, NoError>.pipe()
    
    /// A reference to the splitViewController's back button so that we can
    /// remove it and put it back on demand.
    fileprivate var backButton: UIBarButtonItem?

    /// The view that contains the page text.
    @IBOutlet var textView: UITextView!
    
    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Save the back button so we can restore it after editing.
        // This is necessary because self.splitViewController is nil. It seems
        // like it shouldn't be though.
        backButton = navigationItem.leftBarButtonItem
        textView.dataDetectorTypes = [UIDataDetectorTypes.all]
        
        bindUI()
        
        eventObserver.send(value: .viewDidLoad)
    }
    
    deinit {
        navigationItem.leftBarButtonItems?.forEach { $0.reactive.pressed = nil }
        navigationItem.rightBarButtonItems?.forEach { $0.reactive.pressed = nil }
    }
    
    // MARK: - Helpers

    /// Binds the various UI elements in this view to the viewModel.
    func bindUI() {
        
        // Title
        reactive.title <~ viewModelSignal.map { (state: PageSystem.State) -> String? in String(state.pagePath?.split(separator: "/").last ?? "") }

        /// Page Text
        textView.reactive.text <~ viewModelSignal
            .filter { $0.isEditing == false }
            .map { $0.savedText }
        
        textView.reactive.makeBindingTarget { $0.isEditable = $1 } <~ viewModelSignal.map { $0.isEditing }
        
        /// Bar button items
        navigationItem.reactive.hidesBackButton <~ viewModelSignal.map { $0.isEditing }
        barButtonsInEditMode <~ viewModelSignal.map { $0.isEditing }
    }
    
    /// A binding target that will put the navigation bar buttons into edit mode
    /// when true, or back to non-edit mode when false.
    var barButtonsInEditMode: BindingTarget<Bool> {
        return reactive.makeBindingTarget { [weak self] in
            self?.rebuildNavigationBar(isEditing: $1)
        }
    }
    
    /// Puts the correct bar buttons into the navigation bar.
    /// - parameter isEditing: True if you wish to show the bar buttons for
    ///     editing, false otherwise.
    func rebuildNavigationBar(isEditing: Bool) {
        
        // Dispose of old bar button actions
        navigationItem.leftBarButtonItems?.filter { $0 != self.backButton }.forEach { $0.reactive.pressed = nil }
        navigationItem.rightBarButtonItems?.forEach { $0.reactive.pressed = nil }
        
        if isEditing {
            
            let saveButton = UIBarButtonItem.init(title: "Save", style: .plain, target: nil, action: nil)
            
            // Bind the saveButtonPressed event to the save button
            saveButton.reactive.pressed = CocoaAction(Action() { _ in
                SignalProducer( { [weak self] in
                    self?.eventObserver.send(value: .saveButtonPressed(self?.textView.text))
                })
            })
            
            saveButton.reactive.isEnabled <~ viewModelSignal.map { $0.currentText == nil }
            
            let cancelButton = UIBarButtonItem.init(title: "Cancel", style: .plain, target: nil, action: nil)
            
            /// Bind the cancelButtonPressed event to the cancel button.
            cancelButton.reactive.pressed = CocoaAction(Action() {
                SignalProducer( { [weak self] in
                    self?.eventObserver.send(value: .cancelButtonPressed)
                })
            })

            navigationItem.leftBarButtonItem = cancelButton
            navigationItem.rightBarButtonItem = saveButton
            
        } else {
            
            let editButton = UIBarButtonItem.init(title: "Edit", style: .plain, target: nil, action: nil)
            
            /// Bind the editButtonPressed event to the edit button.
            editButton.reactive.pressed = CocoaAction(Action() {
                SignalProducer( { [weak self] in
                    self?.eventObserver.send(value: .editButtonPressed)
                })
            })
            
            navigationItem.leftBarButtonItem = backButton
            navigationItem.rightBarButtonItem = editButton
        }
        
    }
}

