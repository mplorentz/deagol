//
//  AppDelegate.swift
//  Deagol
//
//  Created by Matthew Lorentz on 3/12/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

import UIKit
import CoreData
import ReactiveSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var appSystem: Property<AppSystem.State>?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        guard let window = window else {
            return false
        }
        
        setGlobalAppearance()
        
        let dataStore = DataStore()
        
        appSystem = AppSystem.build(in: window, dataStore: dataStore)

        return true
    }
}

