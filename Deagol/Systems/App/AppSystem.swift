//
//  AppSystem.swift
//  Deagol
//
//  Created by Matthew Lorentz on 3/14/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveFeedback
import Result
import CoreData

/// A system that runs the Deagol app. It instantiates sub-systems and sometimes
/// passes events between them.
class AppSystem {
    
    /// An enumeration of all the events that can occur in the app system.
    enum Event: Equatable {
        
        /// Signaled when the user has selected a wiki page to view.
        case showPageDetail(Page)
        
        /// Signaled when a wiki page has been displayed to the user.
        case didShowPageDetail
    }
    
    /// A representation of the state of the system.
    struct State {
        
        /// The wiki page that should be displayed to the user immediately.
        var pageToShow: Page?
    }

    /// A pure function that computes new state from state and an event.
    static func reduce(state: State, event: Event) -> State {
        
        var newState = state
        
        switch event {
            
        case .showPageDetail(let page):
            newState.pageToShow = page
        case .didShowPageDetail:
            newState.pageToShow = nil
        }
        
        return newState
    }
    
    // MARK: - Build

    /// Instantiates an `AppSystem` system in the given window with the given
    /// data store.
    ///
    /// parameter window: The window that the appSystem will display its UI.
    ///     The proper view controllers should already have been instantiates
    ///     and arranged in the window by the storyboard.
    /// parameter dataStore: The data store that models should be stored in.
    static func build(
        in window: UIWindow,
        dataStore: DataStore
        ) -> Property<State> {
        
        // Set up split view
        let splitViewController = window.rootViewController as! DeagolSplitViewController
        let splitViewNavController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as! UINavigationController
        splitViewNavController.topViewController!.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem


        // Data
        let api = API(host: "http://157.230.53.153:4567")
        let initialPage = dataStore.initialPage()
        let initialState = PageSystem.State(pagePath: initialPage?.path, savedText: initialPage?.text, isEditing: false, currentText: nil)

        // Page System
        let pageNavController = splitViewController.viewControllers[1] as! UINavigationController
        let pageViewController = pageNavController.viewControllers.first as! PageViewController

        let pageSystem = PageSystem(
            initialState: initialState,
            userInput: pageViewController.eventSignal,
            api: api,
            dataStore: dataStore
        )
        
        /// Take while viewModelObserver is alive
        let systemObserver = pageViewController.viewModelObserver
        pageSystem.state
            .take(duringLifetimeOf: systemObserver)
            .start(systemObserver)

        // Browser System
        let browserNavController = splitViewController.viewControllers[0] as! UINavigationController
        let browserVC = browserNavController.topViewController as! BrowserViewController

        let browserSystem = BrowserSystem(
            userInput: browserVC.eventSignal,
            api: api,
            dataStore: dataStore,
            navigationController: browserNavController
        )
        
        browserVC.state = browserSystem.state

        // Show the browser instead of the pageViewController if we have no page to show.
        if initialPage == nil {
            splitViewController.isFirstLaunch = true
            splitViewController.preferredDisplayMode = .allVisible
        }

        // App System
        return Property(
            initial: State(),
            reduce: reduce,
            feedbacks: [
                AppSystem.navigationFeedback(
                    splitView: splitViewController,
                    pageView: pageViewController,
                    api: api,
                    dataStore: dataStore
                ),
                AppSystem.browserSystemFeedback(browserSystem)
            ]
        )
    }
    
    // MARK: - Feedbacks
    
    /// A feedback that shows the user the `pageToShow` when it is not nil.
    ///
    /// - parameter splitViewController: The split view that we should show the
    ///     page in.
    /// - parameter pageView: The page view that the page should be
    ///     should be shown in. Should be the detail view of `splitView`.
    /// - parameter api: An interface to the server that wiki pages will be
    ///     saved to if they are edited.
    /// - parameter dataStore: The data store that wiki pages will be stored in.
    /// - returns: The feedback.
    static func navigationFeedback(
        splitView: UISplitViewController,
        pageView: PageViewController,
        api: API,
        dataStore: DataStore
        ) -> Feedback<State, Event> {
        
        return Feedback(predicate: { $0.pageToShow != nil }) {
            (state: State) -> SignalProducer<Event, NoError> in
            
            guard let pageToShow = state.pageToShow else {
                // TODO: Return error
                return SignalProducer<Event, NoError>.empty
            }

            let state = PageSystem.State(
                pagePath: pageToShow.path,
                savedText: pageToShow.text,
                isEditing: false,
                currentText: nil
            )
            
            // Wind down the old system
            pageView.eventObserver.sendCompleted()
            pageView.viewModelObserver.sendCompleted()
            
            // Create new pipes for the new system and wire them up.
            // I wish there was a better way to do this.
            let (eventSignal, eventObserver) = Signal<PageSystem.Event, NoError>.pipe()
            pageView.eventSignal = eventSignal
            pageView.eventObserver = eventObserver
            let (viewModelSignal, viewModelObserver) = Signal<PageSystem.State, NoError>.pipe()
            pageView.viewModelSignal = viewModelSignal
            pageView.viewModelObserver = viewModelObserver
            pageView.bindUI()
            
            let newSystem = PageSystem(
                initialState: state,
                userInput: pageView.eventSignal,
                api: api,
                dataStore: dataStore
            )
            
            newSystem.state.start(pageView.viewModelObserver)
            splitView.showDetailViewController(pageView.navigationController!, sender: nil)
            UIView.animate(withDuration: 0.2) {
                splitView.preferredDisplayMode = .primaryHidden
            }

            return Property(value: .didShowPageDetail).producer
        }
    }
    
    /// A feedback that listens to the browser system for events.
    ///
    /// - parameter browserSystem: The `BrowserSystem` to observe.
    /// - returns: the Feedback.
    static func browserSystemFeedback(_ browserSystem: BrowserSystem) -> Feedback<State, Event> {
        
        return Feedback() { _ in browserSystem.appEvents }
    }
}
