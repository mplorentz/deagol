//
//  Colors.swift
//  Deagol
//
//  Created by Matthew Lorentz on 3/23/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

import UIKit

struct Colors {
    static let primary = UIColor.black
    static let accent1 = UIColor.init(red: 248/255, green: 60/255, blue: 158/255, alpha: 1)
}

/// Sets default colors and fonts.
func setGlobalAppearance() {
    
    UINavigationBar.appearance().titleTextAttributes = [
        NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 21),
        NSAttributedString.Key.foregroundColor : Colors.primary,
    ]
    
    UIBarButtonItem.appearance().setTitleTextAttributes(
        [
            NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 17),
            NSAttributedString.Key.foregroundColor : Colors.accent1,
        ], for: .normal
    )

}

class DeagolNavigationController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Erase navigation bar line
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
    }
}
