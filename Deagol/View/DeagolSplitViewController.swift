//
//  DeagolSplitViewController.swift
//  Deagol
//
//  Created by Matthew Lorentz on 4/1/19.
//  Copyright © 2019 Matthew Lorentz. All rights reserved.
//

import UIKit

class DeagolSplitViewController: UISplitViewController, UISplitViewControllerDelegate {
    var isFirstLaunch = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
    }
    
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        return isFirstLaunch
    }
}
